<html>

<body>
    <?php if ($errors->any()) : ?>
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    <?php endif; ?>
    <form action="{{ route('login.action') }}" method="POST">@csrf
        <input type="text" name="username" value="{{ old('username') }}">
        <input type="password" name="password">
        <input type="submit" value="submit">
    </form>
</body>

</html>