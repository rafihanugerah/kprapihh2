<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ParseMiddleware extends Model
{
    use HasFactory;

    public static function login($body)
    {
        $curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => 'https://parseapi.back4app.com/classes/CLASS',
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => '',
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => 'POST',
  CURLOPT_POSTFIELDS =>'{ "Username":"A string","Password":"A string" }',
  CURLOPT_HTTPHEADER => array(
    'X-Parse-Application-Id: oeaUUmQjy9f6lPEyjARmgAFpV5rzQhl4VLYDxrcY',
    'X-Parse-REST-API-Key: 90n78bIYNnBfy4mdvYLOXQaxQKo8gttjzg7BsR5r',
    'Content-Type: application/json'
  ),
));

$response = curl_exec($curl);

curl_close($curl);
echo $response;
    }
}
